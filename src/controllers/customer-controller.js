let Customer = require('../models/customer');

module.exports = {
  getList: getList,
  get: get,
  addMembershipFee: addMembershipFee,
  createUser: createUser,
  updateUser: updateUser,
  login: login,
  logout: logout
};

function getList(req, res) {
  let selects = [];
  let query = Customer.find();
  if (req.query.serialize_group === 'dropdown') {
    query.select(['_id', 'name.first', 'name.last']);
  }
  if (req.params.limit) {
    query.limit(Math.min(limit, 20));
  }
  query.exec().then(
    customers => {
      res.json(customers).end();
    }
  ).catch(
    error => {
      res.status(500).end();
    }
  )
}

function get(req, res) {
  Customer.findById(req.params.id).populate({
    path: 'loans',
    options: {
      select: ['-customer']
    }
  }).exec().then(
    customer => {
      if (customer) {
        res.json(customer).end();
        return;
      }
      res.status(404).json({}).end();
    }
  );
}

function addMembershipFee(req, res) {
  Customer.findByIdAndUpdate(
    req.body.customer_id,
    {
      $push: { 'membershipFee.deposits': { amount: req.body.total, date: req.body.date } },
      $inc: { 'membershipFee.total': req.body.total }
    },
    {
      new: true
    }
  ).exec()
    .then(
      customer => {
        res.status(204).json(customer).end();
      }
    )
    .catch(
      error => {
        res.status(500).json({ error: error.message }).end();
      }
    )
}

function createUser(req, res) {
  let body = req.body;
  let customer = new Customer({
    name: {
      first: body.name.first,
      last: body.name.last,
    },
    stocks: body.stocks,
    joinDate: body.join_date,
    membershipFee: {
      total: body.initial_membership_fee
    },
    payments: {
      total: body.initial_installments_amount,
      date: new Date()
    },
    mobile: body.mobile,
    fatherName: body.father_name,
    nationalId: body.national_id,
    password: body.password
  });
  customer.validate().then(
    () => {
      res.status(201).json(customer).end();
      customer.save()
        .catch(err => {
          if (err) {
            Customer.findByIdAndRemove(customer._id);
          }
        });
    }).catch(
      error => {
        if (error) {
          console.log(`customer validation error: ${error}`.bgRed);
          res.status(400).json({ error: error }).end();
        }
      });
}

function updateUser(id, requestParams) {
  // TODO: validate inputs. find user, 404 if couldn't find. save user new details
}

function login(username, password) {
  // TODO: find user with requested username and password. 404 if couldn't find. create token
}

function logout(username) {
  // TODO: remove user token.
}