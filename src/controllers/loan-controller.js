let mongoose = require('mongoose');
let ObjectId = mongoose.Types.ObjectId;

let Loan = require('../models/loan');
let Customer = require('../models/customer');

module.exports = {
  add: add,
  get: get,
  getList: getList
};

function get(req, res) {
  Loan.findById(req.params.id).exec().then(
    loan => {
      res.json(loan).end();
    }
  );
}

function getList(req, res) {
  let selects = [
    'totalPrice',
    'installmentsQty',
    'customer.name',
    'payments',
    'chequeId'
  ];
  let query = Loan.find();
  if (req.query.customer) {
    if (!ObjectId.isValid(req.query.customer)) {
      res.status(400).json({ message: 'شناسه مشتری اشتباه است' }).end();
      return;
    }
    query.where('customer._id').equals(ObjectId(req.query.customer));
  }
  query.limit(parseInt(req.query.limit) || 10);
  query.skip(parseInt(req.query.offset) || 0);
  if (req.query.unpaid) {
    query.limit(1);
    query.and({ 'paid': false });
  }
  query.select(selects).limit(limit).exec().then(
    loans => {
      res.json(loans).end();
    }
  );
}

function add(req, res, next) {
  let body = req.body;
  // calculations
  let accountant_share = body.total_price / 100;
  let fund_share = body.total_price / 100;
  let normalPayment = parseInt(body.total_price / body.installments_qty / 1000) * 1000;
  let last_payment = body.total_price - (normalPayment * (body.installments_qty - 1));

  let loan = new Loan({
    totalPrice: body.total_price,
    installmentsQty: body.installments_qty,
    accountantShare: accountant_share,
    fundShare: fund_share,
    payments: {
      normal: {
        amount: normalPayment,
        qty: body.installments_qty - 1,
        paidQty: 0
      },
      final: {
        amount: last_payment,
        paid: false
      }
    },
    date: body.date,
    customer: {
      _id: body.customer._id,
      name: body.customer.name,
      stocks: body.customer.stocks
    },
    chequeId: body.cheque_id,
    paid: false
  });
  loan.validate().catch(error => {
    if (error) {
      console.log("error in loan validation: ", error);
      res.status(400).json({ message: 'error in validation' }).end();
      next();
      return;
    }
  });
  loan.save()
    .then(loan => {
      res.json(loan).end();
      addCustomerLoan(loan);
    })
    .catch(error => {
      if (error) {
        console.log('error in saving loan: ', error);
        res.status(500).json({ message: 'error in saving loan' }).end();
        next();
      }
    });
}

function addCustomerLoan(loan) {
  let customerId = loan.customer._id;
  Customer.findByIdAndUpdate(customerId, { $push: { loans: loan._id } }).exec();
}
