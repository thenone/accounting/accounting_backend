let Payment = require('../models/payment');
let Loan = require('../models/loan');
let Customer = require('../models/customer');

module.exports = {
  add: add,
  get: get,
  getList: getList
};

function add(req, res) {
  let body = req.body;
  let installmentType = body.installment_type;
  let installmentAmount = body.loan.payments[installmentType]['amount'];
  let membershipFee = body.total - installmentAmount;
  let payment = new Payment({
    total: body.total,
    loan: body.loan._id,
    installment: {
      type: installmentType,
      amount: installmentAmount
    },
    customer: {
      _id: body.loan.customer._id,
      name: body.loan.customer.name
    },
    docId: body.docId,
    membershipFee: membershipFee,
    date: body.date,
    extra: body.extra
  });
  payment.validate()
    .catch(
      error => { console.log("error in validation:\n", error); }
    );
  payment.save()
    .then(
      savedPayment => {
        res.status(201).json(savedPayment).end();
        updateLoan(req.body, savedPayment);
        updateCustomer(req.body, savedPayment);
      }
    )
    .catch(
      error => { if (error) console.log("error in saving payment:\n", error); }
    );
}

function get(req, res) {
  Payment.findById(req.params.id).populate('loan').exec()
    .then(
      payment => {
        if (payment) {
          res.json(payment).end();
          return;
        }
        res.status(404).json({}).end();
      }
    );
}

function getList(req, res) {
  let query = Payment.find();
  query.select('total customer.name installment date membershipFee');
  query.limit(parseInt(req.query.limit) || 10);
  query.skip(parseInt(req.query.offset) || 0);
  query.exec().then(
    payments => {
      res.json(payments).end();
    }
  );
}

function updateLoan(body, payment) {
  let update = {};
  update.$push = { 'payments.payments': payment._id };
  update.$set = {};
  // if this payment is the last one, then its associated loan will be fully paid
  if (body.isLastPayment) {
    update.$set.paid = true;
  }
  if (body.installment_type === 'final') {
    update.$set['payments.final.paid'] = true;
  } else {
    update.$inc = { 'payments.normal.paidQty': 1 };
  }
  Loan.findByIdAndUpdate(payment.loan, update).then();
}

function updateCustomer(body, payment) {
  let update = { $push: { payments: { total: payment.total, date: payment.date } } };
  if (payment.membershipFee) {
    update.$push['membershipFee.deposits'] = { amount: payment.membershipFee, date: payment.date };
    update.$inc = { 'membershipFee.total': payment.membershipFee };
  }
  Customer.findByIdAndUpdate(body.loan.customer._id, update).then();
}
