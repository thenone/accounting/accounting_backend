let mongoose = require('mongoose');

require("./loan");


let customerType = new mongoose.Schema({
  _id: { type: mongoose.Schema.Types.ObjectId, required: true },
  name: {
    first: { type: String, required: true },
    last: { type: String, required: true }
  }
});

let PaymentSchema = new mongoose.Schema({
  total: { type: Number, required: true },
  loan: { type: mongoose.Schema.Types.ObjectId, ref: 'Loan', required: true },
  installment: {
    type: { type: String },
    amount: { type: Number, required: true }
  },
  docId: { type: Number, required: true },
  customer: { type: customerType, required: true },
  date: { type: Date, default: Date.now },
  membershipFee: { type: Number, required: false },
  stockPurchase: { type: Number, default: 0 },
  extra: { type: String, required: false }
});

module.exports = mongoose.model('Payment', PaymentSchema);
