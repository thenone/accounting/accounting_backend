let mongoose = require("mongoose");
let bcrypt = require('bcrypt');

let Loan = require("./loan");

let AdminSchema = new mongoose.Schema({
  name: {
    first: { type: String, required: true },
    last: { type: String, required: true },
  },
  registerDate: { type: Date },
  lastModifiedDate: { type: Date, default: Date.now },
  mobile: { type: Number, required: true },
  nationalId: { type: Number, unique: true },
  stocks: { type: Number, required: true },
  loans: { type: ['Loan'] },
  password: { type: String, required: true }
});

AdminSchema.pre('save', function(done) {
  bcrypt.hash(this.password, 10).then(hashedPassword => {
    this.password = hashedPassword;
    done();
  });
});

module.exports = mongoose.model('Admin', AdminSchema);
