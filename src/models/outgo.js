let mongoose = require("mongoose");

let toLocaleDate = require('../lib/locale-date');

let OutgoSchema = new mongoose.Schema({
  charge: { type: Number, required: true },
  details: { type: String, text: true },
  date: { type: String, default: Date.now, set: toLocaleDate }
});

module.exports = mongoose.model('Outgo', OutgoSchema);
