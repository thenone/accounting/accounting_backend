let mongoose = require("mongoose");
let bcrypt = require('bcrypt');

require("./loan");


let depositType = new mongoose.Schema({
  amount: { type: Number, required: true },
  date: { type: Date, default: Date.now, required: true }
}, { _id: false });

let paymentType = new mongoose.Schema({
  total: { type: Number, required: true },
  date: { type: Date, default: Date.now }
}, { _id: false });

let CustomerSchema = new mongoose.Schema({
  name: {
    first: { type: String, required: true },
    last: { type: String, required: true }
  },
  stocks: { type: Number, required: true },
  joinDate: { type: Date },
  membershipFee: {
    deposits: { type: [depositType], required: true },
    total: { type: Number, required: true }
  },
  payments: { type: [paymentType], required: true },
  lastModifiedDate: { type: Date, default: Date.now },
  birthDate: { type: Date },
  mobile: { type: Number, required: true, unique: true },
  fatherName: { type: String },
  nationalId: { type: Number, unique: true },
  loans: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Loan' }],
  password: { type: String, required: true }
});

CustomerSchema.pre('save', function (done) {
  bcrypt.hash(this.password, 10).then(hashedPassword => {
    this.password = hashedPassword;
    done();
  });
});

module.exports = mongoose.model('Customer', CustomerSchema);
