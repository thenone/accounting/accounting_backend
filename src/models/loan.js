let mongoose = require("mongoose");

let Customer = require('./customer');
require('./payment');


let LoanSchema = new mongoose.Schema({
  totalPrice: { type: Number, required: true },
  installmentsQty: { type: Number, required: true },
  accountantShare: { type: Number, required: true },
  fundShare: { type: Number, required: true },
  payments: {
    payments: { type: [mongoose.Schema.Types.ObjectId], ref: 'Payment', required: true },
    normal: {
      amount: { type: Number, required: true },
      qty: { type: Number, required: true },
      paidQty: { type: Number, require: true }
    },
    final: {
      amount: { type: Number, required: true },
      paid: { type: Boolean, required: true }
    }
  },
  date: { type: Date, required: true },
  chequeId: { type: Number, required: true, unique: true },
  customer: { type: Customer, required: true },
  paid: { type: Boolean, required: true }
});

module.exports = mongoose.model('Loan', LoanSchema);
