let express = require("express");
let router = express.Router();

let {
  CustomerController,
  LoanController,
  PaymentController
} = require('../lib/controllers');

let Validators = require('../lib/validator');
let CustomerValidators = require('../validators/customer-validators');
let LoanValidators = require('../validators/loan-validators');
let PaymentValidators = require('../validators/payment-validators');


router.get('/customers/:id', [
  Validators.paramId,
  CustomerController.get
]);
router.get('/customers', [
  CustomerController.getList
]);
router.post('/customer', [
  CustomerValidators.required,
  CustomerValidators.password,
  CustomerValidators.mobileType,
  CustomerValidators.dates,
  CustomerValidators.names,
  CustomerValidators.unique,
  CustomerController.createUser
]);


router.post('/membership_fee', [
  Validators.required('total', 'customer_id', 'date'),
  Validators.number('total'),
  Validators.bodyId('customer_id'),
  Validators.date('date'),
  CustomerController.addMembershipFee
]);


router.get('/payments/:id', [
  Validators.paramId,
  PaymentController.get
]);
router.get('/payments/', [
  PaymentController.getList
]);
router.post('/payment', [
  PaymentValidators.required,
  PaymentValidators.types,
  PaymentValidators.date,
  Validators.bodyId('loan'),
  PaymentValidators.loan,
  PaymentController.add
]);


router.get('/loans/:id', [
  Validators.paramId,
  LoanController.get
]);
router.get('/loans', [
  LoanController.getList
]);
router.post('/loan', [
  LoanValidators.required,
  LoanValidators.dates,
  Validators.bodyId('customer_id'),
  LoanValidators.chequeId,
  LoanValidators.allPaid,
  LoanValidators.unique,
  LoanValidators.customer,
  LoanController.add
]);

module.exports = router;