let AdminRoutes = require('../routes/admin-routing');
let CustomerRoutes = require('../routes/customer-routing');

module.exports = { AdminRoutes, CustomerRoutes };