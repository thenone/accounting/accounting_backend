let mongoose = require('mongoose');
let colors = require('colors');

// Connection configurations
const HOST = 'localhost';
const PORT = '27017';
const DB_NAME = 'test';
const USER = '';
const PASSWORD = '';

// Connection
mongoose.connect(`mongodb://${HOST}:${PORT}/${DB_NAME}`);

// EVENTS
// When successfully connected
mongoose.connection.on('connected', () => {
    console.log(`Database Successfully Connected ${HOST}:${PORT}`.bgGreen.black);
});

// If error occurs
mongoose.connection.on('error', (err) => {
    console.log(`\nDatabase Connection Error: ${err}`.bgRed.black);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', () => {
    console.log(`\nDatabase Disconnected`.bgWhite.black);
});

// If the Node process ends, close the Mongoose connection 
process.on('SIGINT', () => {
    mongoose.connection.close(() => {
        console.log('Database Connection Closed Through App Termination'.bgWhite.black);
        process.exit(0);
    });
}); 
