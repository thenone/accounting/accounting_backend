let CustomerController = require('../controllers/customer-controller');
let LoanController = require('../controllers/loan-controller');
let PaymentController = require('../controllers/payment-controller');
let OutgoController = require('../controllers/outgo-controller');

module.exports = {
  CustomerController,
  LoanController,
  PaymentController,
  OutgoController
};