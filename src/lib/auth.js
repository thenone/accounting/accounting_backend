let fs = require('fs');
let bcrypt = require('bcrypt');
let mongoose = require('mongoose');
let jwt = require('jsonwebtoken');

let CustomerSchema = require('../models/customer');
let AdminSchema = require('../models/admin');

const SECRET = fs.readFileSync('./private.key');

module.exports = {
  login: login,
  logout: logout,
  verify: verify
};

function login(req, res) {
  // A tricky way to find admin!
  if (req.body.password.indexOf('admin') === 0) adminLogin(req, res);
  else customerLogin(req, res);
}

function customerLogin(req, res) {
  let customer = mongoose.model('Customer', CustomerSchema);
  let query = customer.findOne();
  query.where('mobile').equals(req.body.username);
  query.select('_id password');
  query.exec().then(customer => {
    if (customer && req.body.password) {
      bcrypt.compare(req.body.password, customer.password)
        .then(correctPass => {
          if (correctPass) {
            let token = jwt.sign({
              id: customer._id
            }, SECRET, { expiresIn: '1h' });
            res.json({ token: token, id: customer._id, exp: oneHourLater() }).end();
          }
          res.status(401).end();
        });
    } else {
      res.status(401).end();
    }
  });
}

function adminLogin(req, res) {
  let admin = mongoose.model('Admin', AdminSchema);
  let query = admin.findOne();
  query.where('mobile').equals(req.body.username);
  query.select('_id password');
  query.exec().then(admin => {
    if (admin && req.body.password) {
      bcrypt.compare(req.body.password.substr(5), admin.password)
        .then(correctPass => {
          if (correctPass) {
            let token = jwt.sign({
              id: admin._id,
              isAdmin: true
            }, SECRET, { expiresIn: '1h' });
            res.json({ token: token, id: admin._id, is_admin: true, exp: oneHourLater() }).end();
          }
          res.status(401).end();
        });
    } else {
      res.status(401).end();
    }
  });
}

function logout(req, res) { }

function verify(req, res) { }

function oneHourLater() {
  let date = new Date();
  date.setUTCHours(date.getUTCHours() + 1);
  return date;
}