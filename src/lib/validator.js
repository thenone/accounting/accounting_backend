let mongoose = require('mongoose');
let ObjectId = mongoose.Types.ObjectId;

module.exports = {
  required: required,
  length: length,
  minLength: minLength,
  mobile: mobile,
  bodyId: bodyId,
  paramId: paramId,
  number: number,
  date: date
};

function required(...parameters) {
  return function (req, res, next) {
    for (let parameter of parameters) {
      if (!req.body[parameter]) {
        res.status(400).json({ message: `${parameter} is required` }).end();
        return;
      }
    }
    next();
  }
}

function length(parameter, length) {
  return function (req, res, next) {
    if (req.body[parameter].length !== length) {
      res.status(400).json({ message: `invalid ${parameter} length of ${length}` }).end();
      return;
    }
    next();
  }
}

function minLength(parameter, minLength) {
  return function (req, res, next) {
    if (!req.body[parameter] || req.body[parameter].length < minLength) {
      res.status(400).json({ message: `${parameter} is less that ${minLength} characters` }).end();
      return;
    }
    next();
  }
}

function mobile(parameter) {
  return function (req, res, next) {
    if (req.body[parameter].charAt(0) !== '9' || req.body[parameter].length !== 10) {
      res.status(400).json({ message: `${parameter} should be of type mobile number` });
      return;
    }
    next();
  }
}

function bodyId(field) {
  return function(req, res, next) {
    if (!ObjectId.isValid(req.body[field])) {
      res.status(400).json({ message: `invalid id for ${field}` }).end();
      return;
    }
    next();
  }
}

function paramId(req, res, next) {
  if (!ObjectId.isValid(req.params.id)) {
    res.status(400).json({ message: `invalid id` }).end();
    return;
  }
  next();
}

function number(...fields) {
  return function (req, res, next) {
    for (const field of fields) {
      if (isNaN(req.body[field])) {
        req.status(400).json({ message: `${field} باید عدد باشد` }).end();
        return;
      }
    }
    next();
  }
}

function date(field) {
  return function (req, res, next) {
    let ISODateRegex = new RegExp(/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z/);
    let date = req.body[field];
    if (date.length > 0 && (isNaN(Date.parse(date)) || !ISODateRegex.test(date))) {
      res.status(400).json({ message: 'تاریخ نامعتبر' }).end();
      return;
    }
    next();
  }
}
