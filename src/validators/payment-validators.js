let mongoose = require('mongoose');
let Loan = require('../models/loan');

module.exports = {
  required: required,
  types: types,
  date: date,
  loan: loan
};

function required(req, res, next) {
  let body = req.body;
  if (
    body.total &&
    body.loan &&
    body.docId &&
    body.date
  ) {
    next();
    return;
  }
  res.status(400).json({ message: 'فرم نامعتبر' }).end();
}

function types(req, res, next) {
  let error;
  if (isNaN(req.body.total)) {
    error = 'مبلغ نامعتبر';
  } else if (req.body.installment_type !== 'normal' && req.body.installment_type !== 'final') {
    error = 'نوع واریزی نامعتبر';
  } else if (isNaN(req.body.docId)) {
    error = 'شماره سند نامعتبر';
  }
  if (error) {
    res.status(400).json({ message: error }).end();
    return;
  }
  next();
}

function date(req, res, next) {
  let ISODateRegex = new RegExp(/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z/);
  if (isNaN(Date.parse(req.body.date)) || !ISODateRegex.test(req.body.date)) {
    res.status(400).json({ message: 'تاریخ نامعتبر' }).end();
    return;
  }
  next();
}

async function loan(req, res, next) {
  let selectedFields = [
    'totalPrice',
    'installmentsQty',
    'payments',
    'customer._id',
    'customer.name'
  ];
  let loan = await Loan.findById(req.body.loan).select(selectedFields).exec();
  if (!loan) {
    res.status(404).json({ message: 'وام یافت نشد' }).end();
    return;
  }
  let installmentType = req.body.installment_type;
  if (installmentType === 'normal' && loan.payments.normal.qty === loan.payments.normal.paidQty) {
    res.status(400).json({ message: 'قسط‌های ثابت وام پرداخت شده‌اند' }).end();
    return;
  }
  if (installmentType === 'final' && loan.payments.final.paid) {
    res.status(400).json({ message: 'قسط پایانی وام پرداخت شده است' }).end();
    return;
  }
  if (req.body.total < loan.payments[installmentType]['amount']) {
    res.status(400).json({ message: 'از مبلغ قسط کمتر است' }).end();
    return;
  }
  if (
    (installmentType === 'normal' && loan.payments.normal.qty === loan.payments.normal.paidQty + 1 && loan.payments.final.paid) ||
    (installmentType === 'final' && loan.payments.normal.qty === loan.payments.normal.paidQty)
  ) {
    req.body.isLastPayment = true;
  }
  req.body.loan = loan;
  next();
}
