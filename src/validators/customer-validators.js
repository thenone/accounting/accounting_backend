let mongoose = require('mongoose');

let Customer = require('../models/customer');

module.exports = {
  required: required,
  password: password,
  mobileType: mobileType,
  dates: dates,
  names: names,
  unique: unique
}

function required(req, res, next) {
  let body = req.body;
  if (
    body.mobile &&
    body.name &&
    body.name.first &&
    body.name.last &&
    body.stocks &&
    body.initial_membership_fee &&
    body.initial_installments_amount &&
    body.national_id &&
    body.password &&
    body.password.first &&
    body.password.confirm
  ) {
    next();
    return;
  }
  res.status(400).json({ message: 'فرم نامعتبر' }).end();
  return;
}

function password(req, res, next) {
  if (req.body.password.first.length >= 6 && req.body.password.confirm.length >= 6 && req.body.password.first == req.body.password.confirm) {
    req.body.password = req.body.password.first;
    next();
    return;
  }
  res.status(400).json({ message: 'رمز نامعتبر' }).end();
}

function mobileType(req, res, next) {
  let mobile = req.body.mobile;
  if (mobile.length === 10 && mobile.charAt(0) === '9') {
    next();
    return;
  }
  res.status(400).json({ message: 'شماره موبایل نامعتبر' }).end();
}

function dates(req, res, next) {
  let ISODateRegex = new RegExp(/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z/);
  let joinDate = req.body.join_date;
  let birthDate = req.body.birth_date;
  if (
    (joinDate && (isNaN(Date.parse(joinDate)) || !ISODateRegex.test(joinDate))) ||
    (birthDate && (isNaN(Date.parse(birthDate)) || !ISODateRegex.test(birthDate)))
  ) {
    res.status(400).json({ message: 'تاریخ نامعتبر' }).end();
    return;
  }
  next();
}

function names(req, res, next) {
  let numRegex = RegExp(/\d/);
  if (numRegex.test(req.body.name.first) || numRegex.test(req.body.name.last)) {
    res.status(400).json({ message: 'نام نامعتبر' }).end();
    return;
  }
  next();
}

async function unique(req, res, next) {
  let duplicates = await Customer.find({ $or: [{ mobile: req.body.mobile }, { nationalId: req.body.national_id }] }).select('id').limit(1).exec();
  if (duplicates && duplicates.length > 0) {
    res.status(400).json({ message: 'موبایل یا شناسه ملی تکراری' }).end();
    return;
  }
  next();
}
