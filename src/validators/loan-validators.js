let mongoose = require('mongoose');
let Loan = require('../models/loan');
let Customer = require('../models/customer');

module.exports = {
  required: required,
  dates: dates,
  chequeId: chequeId,
  allPaid: allPaid,
  unique: unique,
  customer: customer
}

function required(req, res, next) {
  let body = req.body;
  if (
    body.total_price &&
    body.installments_qty &&
    body.date &&
    body.cheque_id &&
    body.customer_id
  ) {
    next();
    return;
  }
  res.status(400).json({ message: 'فرم نامعتبر' }).end();
}

function dates(req, res, next) {
  let ISODateRegex = new RegExp(/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z/);
  let date = req.body.date;
  if (isNaN(Date.parse(date)) || !ISODateRegex.test(date)) {
    res.status(400).json({ message: 'تاریخ نامعتبر' }).end();
    return;
  }
  next();
}

function chequeId(req, res, next) {
  if (isNaN(req.body.cheque_id) || req.body.cheque_id.length < 6) {
    res.status(400).json({ message: 'شماره جک نامعتبر' }).end();
    return;
  }
  next();
}

function allPaid(req, res, next) {
  Loan.find().select('-_id installmentsQty').where('paid').equals(false).where('customer._id').equals(req.body.customer_id).limit(1).exec().then(
    notPaidLoan => {
      if (notPaidLoan && notPaidLoan.length > 0) {
        res.status(400).json({ message: 'کاربر وام تسویه نشده دارد' }).end();
        return;
      }
      next();
    }
  );
}

async function unique(req, res, next) {
  let duplicate = await Loan.find({ chequeId: req.body.cheque_id }).select('id').limit(1).exec();
  if (duplicate && duplicate.length > 0) {
    res.status(400).json({ message: 'شماره چک تکراری' }).end();
    return;
  }
  next();
}

async function customer(req, res, next) {
  let body = req.body;
  let selectFields = ['id', 'name', 'stocks', 'fatherName'];
  let customer = await Customer.findById(body.customer_id).select(selectFields).exec();
  if (!customer || customer.length === 0) {
    res.status(404).json({ message: 'مشتری یافت نشد' }).end();
    return;
  }
  req.body.customer = customer;
  next();
}
