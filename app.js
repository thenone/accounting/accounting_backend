let express = require('express');
let app = express();
let colors = require('colors');

let db = require('./src/lib/db');
let Validator = require('./src/lib/validator');

// Server Configuration
const PORT = 3000;
const HOST = 'localhost';

// Routes
let Routes = require('./src/lib/routes');
let Auth = require('./src/lib/auth');

app.use(express.static('public/html'));
app.use(express.urlencoded({ extended: true }));

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
  if ('OPTIONS' === req.method) {
    //respond with 200
    res.status(200).end();
  } else {
    next();
  }
});

app.post('/login', [
  Validator.required('password', 'username'),
  Validator.minLength('password', 6),
  Validator.mobile('username'),
  Auth.login
]);

app.use('/admin', Routes.AdminRoutes);
app.use('/', Routes.CustomerRoutes);

// Not Found
app.use((req, res) => {
  res.redirect(301, '/404');
});

app.listen(PORT, () => {
  console.log(`Server Is Running On ${HOST}:${PORT}`.bgGreen.black);
});